

echo Make sure to install the OpenGL subsystem

echo SDL is required.

exit


ii  libsdl-gfx1.2-5:amd64                         2.0.25-5                                   amd64        drawing and graphical effects extension for SDL
ii  libsdl-gfx1.2-dev:amd64                       2.0.25-5                                   amd64        development files for SDL_gfx
ii  libsdl-image1.2:amd64                         1.2.12-5+deb9u2                            amd64        Image loading library for Simple DirectMedia Layer 1.2, libraries
ii  libsdl-image1.2-dev:amd64                     1.2.12-5+deb9u2                            amd64        Image loading library for Simple DirectMedia Layer 1.2, development files
ii  libsdl-mixer1.2:amd64                         1.2.12-11+b3                               amd64        Mixer library for Simple DirectMedia Layer 1.2, libraries
ii  libsdl-mixer1.2-dev:amd64                     1.2.12-11+b3                               amd64        Mixer library for Simple DirectMedia Layer 1.2, development files
ii  libsdl-net1.2:amd64                           1.2.8-4                                    amd64        Network library for Simple DirectMedia Layer 1.2, libraries
ii  libsdl-net1.2-dev:amd64                       1.2.8-4                                    amd64        Network library for Simple DirectMedia Layer 1.2, development files
ii  libsdl-sound1.2:amd64                         1.0.3-7+b3                                 amd64        Sound library for Simple DirectMedia Layer 1.2, libraries
ii  libsdl-sound1.2-dev:amd64                     1.0.3-7+b3                                 amd64        Sound library for Simple DirectMedia Layer 1.2, development files
ii  libsdl-ttf2.0-0:amd64                         2.0.11-3+b1                                amd64        TrueType Font library for Simple DirectMedia Layer 1.2, libraries
ii  libsdl-ttf2.0-dev:amd64                       2.0.11-3+b1                                amd64        TrueType Font library for Simple DirectMedia Layer 1.2, development files
ii  libsdl1.2-dev                                 1.2.15+dfsg1-4                             amd64        Simple DirectMedia Layer development files
ii  libsdl1.2debian:amd64                         1.2.15+dfsg1-4                             amd64        Simple DirectMedia Layer
ii  libsdl2-2.0-0:amd64                           2.0.5+dfsg1-2                              amd64        Simple DirectMedia Layer
ii  libsdl2-dev                                   2.0.5+dfsg1-2                              amd64        Simple DirectMedia Layer development files
ii  libsdl2-gfx-1.0-0:amd64                       1.0.1+dfsg-4                               amd64        drawing and graphical effects extension for SDL2
ii  libsdl2-gfx-dev:amd64                         1.0.1+dfsg-4                               amd64        development files for SDL2_gfx
ii  libsdl2-image-2.0-0:amd64                     2.0.1+dfsg-2+deb9u2                        amd64        Image loading library for Simple DirectMedia Layer 2, libraries
ii  libsdl2-image-dev:amd64                       2.0.1+dfsg-2+deb9u2                        amd64        Image loading library for Simple DirectMedia Layer 2, development files
ii  libsdl2-mixer-2.0-0:amd64                     2.0.1+dfsg1-1                              amd64        Mixer library for Simple DirectMedia Layer 2, libraries
ii  libsdl2-mixer-dev:amd64                       2.0.1+dfsg1-1                              amd64        Mixer library for Simple DirectMedia Layer 2, development files
ii  libsdl2-net-2.0-0:amd64                       2.0.1+dfsg1-2                              amd64        Network library for Simple DirectMedia Layer 2, libraries
ii  libsdl2-net-dev:amd64                         2.0.1+dfsg1-2                              amd64        Network library for Simple DirectMedia Layer 2, development files
ii  libsdl2-ttf-2.0-0:amd64                       2.0.14+dfsg1-1                             amd64        TrueType Font library for Simple DirectMedia Layer 2, libraries
ii  libsdl2-ttf-dev:amd64                         2.0.14+dfsg1-1                             amd64        TrueType Font library for Simple DirectMedia Layer 2, development files



